import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import RidgeCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from sklearn.preprocessing import Imputer
from tpot.builtins import StackingEstimator

df_train = pd.read_csv('data/raw/train.csv')
df_test = pd.read_csv('data/raw/test.csv')

df = pd.concat(objs=[df_train, df_test], axis=0, sort=False)
df_processed = pd.get_dummies(df, dummy_na=True)

df_train_features = df_processed[:len(df_train)].drop(['Id'], axis=1).astype(np.float64)
df_train_targets = df_train_features.pop('SalePrice')
df_test_features = df_processed[len(df_train):].drop('SalePrice', axis=1).astype(np.float64)
df_test_ids = df_test_features.pop('Id').astype(int)

imputer = Imputer(strategy="median")
imputer.fit(df_train_features)
df_train_features = imputer.transform(df_train_features)
df_test_features = imputer.transform(df_test_features)

# Average CV score on the training set was:-696764523.5000432
exported_pipeline = make_pipeline(
    StackingEstimator(estimator=RidgeCV()),
    RandomForestRegressor(bootstrap=False, max_features=0.2, min_samples_leaf=1, min_samples_split=14, n_estimators=100)
)

exported_pipeline.fit(df_train_features, df_train_targets.values)
results = exported_pipeline.predict(df_test_features)

df_submission = pd.concat([df_test_ids, pd.Series(results)], axis=1)
df_submission.columns = ['Id', 'SalePrice']
df_submission.to_csv('models/submission.csv', index=False)
