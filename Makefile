.PHONY: environment clean download data
$(VERBOSE).SILENT:

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
INTERPRETER = poetry run python3

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Install Python environment dependencies
environment:
	poetry install

## Delete all compiled and generated files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete
	find data -type f -name "*.csv" -delete

KAGGLE_COMPETITION_CONTENT = data/raw/train.csv \
                             data/raw/test.csv \
                             models/sample_submission.csv \
                             references/data_description.txt
$(KAGGLE_COMPETITION_CONTENT):
	poetry run kaggle competitions download house-prices-advanced-regression-techniques -f $(notdir $@) -p $(dir $@)
## Download competition content
download: $(KAGGLE_COMPETITION_CONTENT)

PROCESSED_DATA_CONTENT = data/processed/train.csv \
                         data/processed/test.csv
$(PROCESSED_DATA_CONTENT):
	$(INTERPRETER) src/data/make_dataset.py data/raw/$(notdir $@) $@
## Produce processed data files
data: download $(PROCESSED_DATA_CONTENT)

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################

## Launch Jupyter notebook
jupyter:
	poetry run jupyter notebook

#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
